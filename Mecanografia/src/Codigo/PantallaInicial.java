package Codigo;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import net.miginfocom.swing.MigLayout;
import javax.swing.Timer;
import javax.swing.border.*;

public class PantallaInicial {

	public JFrame frmMenu;
	// Llama a la clase hija (Teclado)
	PantallaPrincipal hija = new PantallaPrincipal(this);
	public static JLabel lblUsuarioEst;
	public JTextArea txtEstadisticas;
	
	private Timer timer;
	public static String[] nombreFicheros = {"usuarios.txt", "leccion.txt", "estadisticas.txt"};
	private Fichero fichero = new Fichero(".//src//Media//" + nombreFicheros[0], 0);
	private int numF = 0;
	public static String usuario;
	public static int leccion;
	public int leccionEst;
	private final ButtonGroup btnGroup = new ButtonGroup();

	// Main
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaInicial window = new PantallaInicial();
					window.frmMenu.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaInicial() {
		hija.frmPP.setVisible(false);
		
		initialize();
		timer.start();
		leccionEst = 1;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		// Frame
		frmMenu = new JFrame();
		frmMenu.getContentPane().setBackground(new Color(139, 69, 19));
		frmMenu.setTitle("Mecanograf\u00EDa Pepe. Aprende con nosotros.");
		frmMenu.setIconImage(Toolkit.getDefaultToolkit().getImage(PantallaInicial.class.getResource("/Media/icono.png")));
		frmMenu.setResizable(false);
		frmMenu.setBounds(100, 100, 700, 500);
		frmMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMenu.setLocationRelativeTo(null);
		frmMenu.getContentPane().setLayout(new MigLayout("", "[85.00,grow][391.00px,fill][grow]", "[136.00px][254.00,grow,fill][30.00,grow]"));
		
		// Titulo
		JLabel Titulo = new JLabel("Mecanograf\u00EDa Pepe");
		Titulo.setForeground(Color.YELLOW);
		Titulo.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
		Titulo.setHorizontalAlignment(SwingConstants.CENTER);
		frmMenu.getContentPane().add(Titulo, "cell 1 0,alignx center,aligny center");
		
		// BTN Acerca de
		JButton btnAcercaDe = new JButton("Acerca de");
		frmMenu.getContentPane().add(btnAcercaDe, "cell 2 0,alignx right,aligny top");
		
		// Contenedor de los paneles de carga, login, selector, lecciones y estadisticas
		JPanel panel = new JPanel();
		frmMenu.getContentPane().add(panel, "cell 1 1,grow");
		panel.setLayout(new CardLayout(0, 0));
		
		// Carga del programa
		JPanel panelEspera = new JPanel();
		panelEspera.setBackground(new Color(139, 69, 19));
		panel.add(panelEspera, "name_66249201988000");
		panelEspera.setLayout(new MigLayout("", "[344.00px,grow,center]", "[30%,grow][55.00,grow][30%,grow]"));
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		progressBar.setForeground(new Color(30, 144, 255));
		progressBar.setFont(new Font("Tahoma", Font.PLAIN, 13));
		panelEspera.add(progressBar, "cell 0 1,grow");
		
		// Login
		JPanel panelLogin = new JPanel();
		panelLogin.setBackground(new Color(139, 69, 19));
		panelLogin.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		panelLogin.setVisible(false);
		panel.add(panelLogin, "name_66250769496900");
		panelLogin.setLayout(new MigLayout("", "[15%,grow][70%,grow,center][15%,grow]", "[20%,grow][20%][20%,grow][20%][20%]"));
		
		JLabel txtUser = new JLabel("Usuario:");
		txtUser.setHorizontalAlignment(SwingConstants.LEFT);
		txtUser.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 20));
		panelLogin.add(txtUser, "cell 1 0,alignx left,aligny center");
		
		JTextField inputUser = new JTextField();
		inputUser.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 18));
		panelLogin.add(inputUser, "cell 1 1,grow");
		inputUser.setColumns(10);
		
		JLabel txtPass = new JLabel("Contrase\u00F1a:");
		txtPass.setHorizontalAlignment(SwingConstants.LEFT);
		txtPass.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 20));
		panelLogin.add(txtPass, "cell 1 2,alignx left,aligny center");
		
		JPasswordField inputPass = new JPasswordField();
		inputPass.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 18));
		inputPass.setHorizontalAlignment(SwingConstants.LEFT);
		panelLogin.add(inputPass, "cell 1 3,grow");
		
		JButton btnEntrar = new JButton("Entrar");
		panelLogin.add(btnEntrar, "cell 1 4,alignx center,aligny bottom");
		
		// Selector de pratica o estadisticas
		JPanel panelSelector = new JPanel();
		panelSelector.setBackground(new Color(139, 69, 19));
		panelSelector.setVisible(false);
		panel.add(panelSelector, "name_75397851706300");
		panelSelector.setLayout(new MigLayout("", "[28%,grow,fill][44%,fill][28%,grow,right]", "[grow][fill][grow][fill][grow][center]"));
		
		JButton btnPractica = new JButton("Ir a Practicar");
		btnPractica.setFont(new Font("Times New Roman", Font.BOLD, 16));
		panelSelector.add(btnPractica, "cell 1 1,grow");
		
		JButton btnEstadisticas = new JButton("Ver Estad\u00EDsticas");
		btnEstadisticas.setFont(new Font("Times New Roman", Font.BOLD, 16));
		panelSelector.add(btnEstadisticas, "cell 1 3,grow");
		
		JButton btnDesconectarse = new JButton("Desconectarse");
		btnDesconectarse.setFont(new Font("Tahoma", Font.BOLD, 12));
		panelSelector.add(btnDesconectarse, "cell 2 5,alignx right");
		
		// Eleccion de las Lecciones
		JPanel panelLeccion = new JPanel();
		panelLeccion.setBackground(new Color(139, 69, 19));
		panel.add(panelLeccion, "name_77556324938600");
		panelLeccion.setLayout(new MigLayout("", "[80.00,grow][48.08%,grow,fill][78.00,grow]", "[grow][20%,fill][grow][20%,fill][grow]"));
		
		JButton btnLeccion1 = new JButton("Leccion 1");
		btnLeccion1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		panelLeccion.add(btnLeccion1, "cell 1 1");
		
		JButton btnLeccion2 = new JButton("Leccion 2");
		btnLeccion2.setFont(new Font("Times New Roman", Font.BOLD, 18));
		panelLeccion.add(btnLeccion2, "cell 1 3");
		
		JButton btnVolverLeccion = new JButton("Volver");
		btnVolverLeccion.setFont(new Font("Tahoma", Font.BOLD, 13));
		panelLeccion.add(btnVolverLeccion, "cell 2 4,growx,aligny bottom");
		
		// Estadisticas
		JPanel panelEstadisticas = new JPanel();
		panelEstadisticas.setBackground(new Color(139, 69, 19));
		panel.add(panelEstadisticas, "name_75653953564900");
		panelEstadisticas.setLayout(new MigLayout("", "[46px,grow,center][grow,center]", "[20px][14px,grow,fill][][]"));
		
		lblUsuarioEst = new JLabel();
		lblUsuarioEst.setHorizontalAlignment(SwingConstants.CENTER);
		lblUsuarioEst.setBorder(new LineBorder(new Color(210, 105, 30), 2, true));
		lblUsuarioEst.setFont(new Font("Times New Roman", Font.BOLD, 20));
		panelEstadisticas.add(lblUsuarioEst, "cell 0 0 2 1,growx");
		
		txtEstadisticas = new JTextArea();
		txtEstadisticas.setWrapStyleWord(true);
		txtEstadisticas.setTabSize(100);
		txtEstadisticas.setRows(5);
		txtEstadisticas.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 14));
		txtEstadisticas.setEditable(false);
		txtEstadisticas.setBackground(new Color(139, 69, 19));
		panelEstadisticas.add(txtEstadisticas, "cell 0 1 2 1,grow");
		
		JButton btnVolverEst = new JButton("Volver");
		panelEstadisticas.add(btnVolverEst, "cell 1 3,alignx right");
		
		JRadioButton rdbtnLeccion1 = new JRadioButton("Leccion 1");
		btnGroup.add(rdbtnLeccion1);
		rdbtnLeccion1.setSelected(true);
		rdbtnLeccion1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnLeccion1.setBackground(new Color(139, 69, 19));
		panelEstadisticas.add(rdbtnLeccion1, "cell 0 2");
		
		JRadioButton rdbtnLeccion2 = new JRadioButton("Leccion 2");
		btnGroup.add(rdbtnLeccion2);
		rdbtnLeccion2.setFont(new Font("Tahoma", Font.PLAIN, 14));
		rdbtnLeccion2.setBackground(new Color(139, 69, 19));
		panelEstadisticas.add(rdbtnLeccion2, "cell 0 3");
		
		// Timer para la progressBar
		timer = new Timer(100, new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            	if (progressBar.getValue() < 50)
            		progressBar.setValue(progressBar.getValue() + 4);
            	else
            		progressBar.setValue(progressBar.getValue() + 6);
            	
            	if ( numF != nombreFicheros.length && !Fichero.comprobarFicheros(".//src//Media//" + nombreFicheros[numF++]) ) {
            		JOptionPane.showMessageDialog(null, "El fichero ("+ nombreFicheros[--numF] +") no se ha encontrado o esta vacio.", "Carga de Ficheros", JOptionPane.ERROR_MESSAGE);
            		frmMenu.dispose();
            		timer.stop();
            		System.exit(0);
            	}
            	
            	if (progressBar.getValue() >= 100) {
            		timer.stop();
            		JOptionPane.showMessageDialog(null, "El programa se ha ejecutado correctamente.", "Carga de Ficheros", JOptionPane.INFORMATION_MESSAGE);
            		panelEspera.setVisible(false);
            		panelLogin.setVisible(true);
            	}
            }
		});
		
		// BTN Volver de estadisticas
		btnVolverEst.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelEstadisticas.setVisible(false);
				panelSelector.setVisible(true);
			}
		});
		
		// BTN Acerca de
		btnAcercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null, "Creador:     Jorge Loarte Smith\nFecha:         15/10/2019\nVersi�n:      1.0.0", "Acerca de", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		
		// BTN Comprobacion de usuarios
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if ( fichero.comprobarUsuarios(inputUser.getText(), inputPass.getText()) ) {
					usuario = inputUser.getText();
					panelLogin.setVisible(false);
					panelSelector.setVisible(true);
				}
				else
					JOptionPane.showMessageDialog(null, "El usuario o la contrase�a son incorrectos.", "Login", JOptionPane.ERROR_MESSAGE);
			}
		});
		
		// BTN Desconectarse
		btnDesconectarse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "�De verdad quieres desconectarte?", "Desconexi�n", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					inputPass.setText("");
					panelSelector.setVisible(false);
					panelLogin.setVisible(true);					
				}
			}
		});
		
		// BTN Ir practica
		btnPractica.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelSelector.setVisible(false);
				panelLeccion.setVisible(true);
			}
		});
		
		// BTN Elegir leccion
		btnLeccion1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leccion = 1;
				hija.fichero.setLeccion(leccion);
				hija.texto.setText(  hija.fichero.getLeccion() );
				hija.fichero.setLongitud();
				hija.leccion = hija.fichero.getLeccion().toCharArray();
				hija.reseteoPP();
				frmMenu.setVisible(false);
				hija.frmPP.setVisible(true);
			}
		});
		btnLeccion2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leccion = 2;
				hija.fichero.setLeccion(leccion);
				hija.texto.setText(  hija.fichero.getLeccion() );
				hija.fichero.setLongitud();
				hija.leccion = hija.fichero.getLeccion().toCharArray();
				hija.reseteoPP();
				frmMenu.setVisible(false);
				hija.frmPP.setVisible(true);
			}
		});
		
		// BTN Volver de leccion
		btnVolverLeccion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelLeccion.setVisible(false);
				panelSelector.setVisible(true);
			}
		});
		
		// BTN Ver estadisticas
		btnEstadisticas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				muestraEstadisticas();
				panelSelector.setVisible(false);
				panelEstadisticas.setVisible(true);
			}
		});
		
		// Coje la leccion de Estadisticas
		rdbtnLeccion1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leccionEst = 1;
				muestraEstadisticas();
			}
		});
		rdbtnLeccion2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				leccionEst = 2;
				muestraEstadisticas();
			}
		});
	}
	
	// Mustra las estadisticas
	private void muestraEstadisticas () {
		
		Estadisticas est = new Estadisticas();
		String[] estadisticas;
		estadisticas = est.getEstadisticasFichero(usuario, leccionEst);
		
		lblUsuarioEst.setText("USUARIO:   " + usuario.toUpperCase());
		txtEstadisticas.setText(
				"\n\n" +
				"    Fecha:  " + estadisticas[0] + "\n"+
				"    Pulsaciones:  " +  estadisticas[1] + "\n" +
				"    Pulsaciones por Minuto:  " + estadisticas[2] + " ppm\n" +
				"    Errores:  " + estadisticas[3] + "\n" +
				"    Tiempo:  " + estadisticas[4]
		);
	}
}
