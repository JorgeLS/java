package Codigo;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.text.*;
import javax.swing.text.DefaultHighlighter.*;

import java.awt.*;
import java.awt.event.*;
import net.miginfocom.swing.MigLayout;

public class PantallaPrincipal {

	public JFrame frmPP;
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	private static PantallaInicial menu;
	public Fichero fichero = new Fichero(".//src//Media//leccion.txt", 1);

	static PantallaInicial inicial;
	public JTextArea texto;
	protected JTextField numPT;
	protected JTextField numPM;
	protected JTextField numErrores;
	protected JTextField numTiempo;

	// Llama a la clase estadisticas
	protected Estadisticas estadisticas = new Estadisticas();
	protected int posicionCursor;
	protected char[] leccion;
	protected boolean error;
	protected Timer timer;
	protected boolean usoTeclado;
	// Colores de los botones
	protected Color normalColor = UIManager.getColor("Button.background");
	protected Color pressColor = UIManager.getColor("Button.foreground");
	// Colores del subrayado
	protected DefaultHighlightPainter hlgood = new DefaultHighlightPainter(Color.GREEN);
	protected DefaultHighlightPainter hlbad = new DefaultHighlightPainter(Color.RED);

	
	public PantallaPrincipal(PantallaInicial menu) {

		initialize();
		
		reseteoPP();

		leccion = fichero.getLeccion().toCharArray();
		texto.setText(fichero.getLeccion());

		this.menu = menu;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frmPP = new JFrame();
		frmPP.setTitle("Mecanograf\u00EDa Pepe. Aprende con nosotros.");

		frmPP.setVisible(true);
		frmPP.setBackground(new Color(139, 69, 19));
		frmPP.setForeground(new Color(139, 69, 19));
		frmPP.setBounds(100, 100, 1078, 670);
		frmPP.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPP.setSize(new Dimension(screenSize.width, screenSize.height));
		frmPP.setResizable(false);
		frmPP.setExtendedState(frmPP.MAXIMIZED_BOTH);

		frmPP.setIconImage(
				Toolkit.getDefaultToolkit().getImage(PantallaPrincipal.class.getResource("/Media/icono.png")));
		frmPP.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		frmPP.getContentPane().setBackground(new Color(139, 69, 19));
		frmPP.getContentPane().setLayout(
				new MigLayout("", "[][grow,fill]", "[416.00px,grow,fill][270.00,grow,fill][75.00,grow,fill]"));
		frmPP.setFocusable(true);

		JPanel Top = new JPanel();
		Top.setBackground(new Color(139, 69, 19));
		frmPP.getContentPane().add(Top, "cell 1 0,grow");
		Top.setLayout(new BorderLayout(0, 0));

		JPanel Title = new JPanel();
		Top.add(Title, BorderLayout.NORTH);
		Title.setBorder(new LineBorder(new Color(184, 134, 11), 2));
		Title.setBackground(new Color(139, 69, 19));
		Title.setLayout(new MigLayout("", "[1632.00px][195.00px][45px]", "[29px]"));

		JLabel titulo = new JLabel("  EJERCICIO    DE    PRUEBA   (SELECCIONADO  DE  FORMA   MANUAL)");
		titulo.setForeground(Color.ORANGE);
		titulo.setFont(new Font("Yu Gothic UI Semibold", Font.BOLD, 25));
		Title.add(titulo, "cell 0 0");

		JButton btnAcercaDe = new JButton("Acerca de");
		Title.add(btnAcercaDe, "cell 1 0,alignx left,aligny center");

		JButton btnExit = new JButton("X");
		btnExit.setAlignmentX(Component.CENTER_ALIGNMENT);
		Title.add(btnExit, "cell 2 0,alignx left,aligny top");
		btnExit.setBackground(new Color(169, 169, 169));
		btnExit.setFont(new Font("Arial Black", Font.BOLD, 14));
		btnExit.setForeground(new Color(255, 0, 0));

		texto = new JTextArea();
		texto.setWrapStyleWord(true);
		texto.setEditable(false);
		texto.setBorder(new LineBorder(new Color(139, 69, 19), 8));
		texto.setAlignmentX(Component.RIGHT_ALIGNMENT);
		Top.add(texto);
		texto.setLineWrap(true);
		texto.setFont(new Font("Times New Roman", Font.PLAIN, 30));

		JPanel Center = new JPanel();
		Center.setBorder(new LineBorder(new Color(205, 133, 63), 3));
		Center.setBackground(new Color(139, 69, 19));
		frmPP.getContentPane().add(Center, "cell 1 1,alignx center,aligny center");
		Center.setLayout(new MigLayout("", "[840.00px,grow][17.25%]", "[234.00,grow]"));

		JPanel Teclado = new JPanel();
		Teclado.setBackground(new Color(139, 69, 19));
		Center.add(Teclado, "cell 0 0,grow");
		GridBagLayout gbl_Teclado = new GridBagLayout();
		gbl_Teclado.columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_Teclado.rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_Teclado.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		gbl_Teclado.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE };
		Teclado.setLayout(gbl_Teclado);

		JButton btn1 = new JButton("1");
		btn1.setBackground(UIManager.getColor("Button.background"));
		btn1.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn1.setEnabled(false);
		GridBagConstraints gbc_btn1 = new GridBagConstraints();
		gbc_btn1.fill = GridBagConstraints.BOTH;
		gbc_btn1.insets = new Insets(0, 0, 5, 5);
		gbc_btn1.gridx = 1;
		gbc_btn1.gridy = 0;
		Teclado.add(btn1, gbc_btn1);

		JButton btn2 = new JButton("2");
		btn2.setBackground(UIManager.getColor("Button.background"));
		btn2.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn2.setEnabled(false);
		GridBagConstraints gbc_btn2 = new GridBagConstraints();
		gbc_btn2.fill = GridBagConstraints.BOTH;
		gbc_btn2.insets = new Insets(0, 0, 5, 5);
		gbc_btn2.gridx = 2;
		gbc_btn2.gridy = 0;
		Teclado.add(btn2, gbc_btn2);

		JButton btn3 = new JButton("3");
		btn3.setBackground(UIManager.getColor("Button.background"));
		btn3.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn3.setEnabled(false);
		GridBagConstraints gbc_btn3 = new GridBagConstraints();
		gbc_btn3.fill = GridBagConstraints.BOTH;
		gbc_btn3.insets = new Insets(0, 0, 5, 5);
		gbc_btn3.gridx = 3;
		gbc_btn3.gridy = 0;
		Teclado.add(btn3, gbc_btn3);

		JButton btn4 = new JButton("4");
		btn4.setBackground(UIManager.getColor("Button.background"));
		btn4.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn4.setEnabled(false);
		GridBagConstraints gbc_btn4 = new GridBagConstraints();
		gbc_btn4.fill = GridBagConstraints.BOTH;
		gbc_btn4.insets = new Insets(0, 0, 5, 5);
		gbc_btn4.gridx = 4;
		gbc_btn4.gridy = 0;
		Teclado.add(btn4, gbc_btn4);

		JButton btn5 = new JButton("5");
		btn5.setBackground(UIManager.getColor("Button.background"));
		btn5.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn5.setEnabled(false);
		GridBagConstraints gbc_btn5 = new GridBagConstraints();
		gbc_btn5.fill = GridBagConstraints.BOTH;
		gbc_btn5.insets = new Insets(0, 0, 5, 5);
		gbc_btn5.gridx = 5;
		gbc_btn5.gridy = 0;
		Teclado.add(btn5, gbc_btn5);

		JButton btn6 = new JButton("6");
		btn6.setBackground(UIManager.getColor("Button.background"));
		btn6.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn6.setEnabled(false);
		GridBagConstraints gbc_btn6 = new GridBagConstraints();
		gbc_btn6.fill = GridBagConstraints.BOTH;
		gbc_btn6.insets = new Insets(0, 0, 5, 5);
		gbc_btn6.gridx = 6;
		gbc_btn6.gridy = 0;
		Teclado.add(btn6, gbc_btn6);

		JButton btn7 = new JButton("7");
		btn7.setBackground(UIManager.getColor("Button.background"));
		btn7.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn7.setEnabled(false);
		GridBagConstraints gbc_btn7 = new GridBagConstraints();
		gbc_btn7.fill = GridBagConstraints.BOTH;
		gbc_btn7.insets = new Insets(0, 0, 5, 5);
		gbc_btn7.gridx = 7;
		gbc_btn7.gridy = 0;
		Teclado.add(btn7, gbc_btn7);

		JButton btn8 = new JButton("8");
		btn8.setBackground(UIManager.getColor("Button.background"));
		btn8.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn8.setEnabled(false);
		GridBagConstraints gbc_btn8 = new GridBagConstraints();
		gbc_btn8.fill = GridBagConstraints.BOTH;
		gbc_btn8.insets = new Insets(0, 0, 5, 5);
		gbc_btn8.gridx = 8;
		gbc_btn8.gridy = 0;
		Teclado.add(btn8, gbc_btn8);

		JButton btn9 = new JButton("9");
		btn9.setBackground(UIManager.getColor("Button.background"));
		btn9.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn9.setEnabled(false);
		GridBagConstraints gbc_btn9 = new GridBagConstraints();
		gbc_btn9.fill = GridBagConstraints.BOTH;
		gbc_btn9.insets = new Insets(0, 0, 5, 5);
		gbc_btn9.gridx = 9;
		gbc_btn9.gridy = 0;
		Teclado.add(btn9, gbc_btn9);

		JButton btn0 = new JButton("0");
		btn0.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn0.setEnabled(false);
		btn0.setBackground(SystemColor.menu);
		GridBagConstraints gbc_btn0 = new GridBagConstraints();
		gbc_btn0.fill = GridBagConstraints.BOTH;
		gbc_btn0.insets = new Insets(0, 0, 5, 0);
		gbc_btn0.gridx = 10;
		gbc_btn0.gridy = 0;
		Teclado.add(btn0, gbc_btn0);

		JButton btnQ = new JButton("Q");
		btnQ.setBackground(UIManager.getColor("Button.background"));
		btnQ.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnQ.setEnabled(false);
		GridBagConstraints gbc_btnQ = new GridBagConstraints();
		gbc_btnQ.fill = GridBagConstraints.BOTH;
		gbc_btnQ.insets = new Insets(0, 0, 5, 5);
		gbc_btnQ.gridx = 1;
		gbc_btnQ.gridy = 1;
		Teclado.add(btnQ, gbc_btnQ);

		JButton btnW = new JButton("W");
		btnW.setBackground(UIManager.getColor("Button.background"));
		btnW.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnW.setEnabled(false);
		GridBagConstraints gbc_btnW = new GridBagConstraints();
		gbc_btnW.fill = GridBagConstraints.BOTH;
		gbc_btnW.insets = new Insets(0, 0, 5, 5);
		gbc_btnW.gridx = 2;
		gbc_btnW.gridy = 1;
		Teclado.add(btnW, gbc_btnW);

		JButton btnE = new JButton("E");
		btnE.setBackground(UIManager.getColor("Button.background"));
		btnE.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnE.setEnabled(false);
		GridBagConstraints gbc_btnE = new GridBagConstraints();
		gbc_btnE.fill = GridBagConstraints.BOTH;
		gbc_btnE.insets = new Insets(0, 0, 5, 5);
		gbc_btnE.gridx = 3;
		gbc_btnE.gridy = 1;
		Teclado.add(btnE, gbc_btnE);

		JButton btnR = new JButton("R");
		btnR.setBackground(UIManager.getColor("Button.background"));
		btnR.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnR.setEnabled(false);
		GridBagConstraints gbc_btnR = new GridBagConstraints();
		gbc_btnR.fill = GridBagConstraints.BOTH;
		gbc_btnR.insets = new Insets(0, 0, 5, 5);
		gbc_btnR.gridx = 4;
		gbc_btnR.gridy = 1;
		Teclado.add(btnR, gbc_btnR);

		JButton btnT = new JButton("T");
		btnT.setBackground(UIManager.getColor("Button.background"));
		btnT.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnT.setEnabled(false);
		GridBagConstraints gbc_btnT = new GridBagConstraints();
		gbc_btnT.fill = GridBagConstraints.BOTH;
		gbc_btnT.insets = new Insets(0, 0, 5, 5);
		gbc_btnT.gridx = 5;
		gbc_btnT.gridy = 1;
		Teclado.add(btnT, gbc_btnT);

		JButton btnY = new JButton("Y");
		btnY.setBackground(UIManager.getColor("Button.background"));
		btnY.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnY.setEnabled(false);
		GridBagConstraints gbc_btnY = new GridBagConstraints();
		gbc_btnY.fill = GridBagConstraints.BOTH;
		gbc_btnY.insets = new Insets(0, 0, 5, 5);
		gbc_btnY.gridx = 6;
		gbc_btnY.gridy = 1;
		Teclado.add(btnY, gbc_btnY);

		JButton btnU = new JButton("U");
		btnU.setBackground(UIManager.getColor("Button.background"));
		btnU.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnU.setEnabled(false);
		GridBagConstraints gbc_btnU = new GridBagConstraints();
		gbc_btnU.fill = GridBagConstraints.BOTH;
		gbc_btnU.insets = new Insets(0, 0, 5, 5);
		gbc_btnU.gridx = 7;
		gbc_btnU.gridy = 1;
		Teclado.add(btnU, gbc_btnU);

		JButton btnI = new JButton("I");
		btnI.setBackground(UIManager.getColor("Button.background"));
		btnI.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnI.setEnabled(false);
		GridBagConstraints gbc_btnI = new GridBagConstraints();
		gbc_btnI.fill = GridBagConstraints.BOTH;
		gbc_btnI.insets = new Insets(0, 0, 5, 5);
		gbc_btnI.gridx = 8;
		gbc_btnI.gridy = 1;
		Teclado.add(btnI, gbc_btnI);

		JButton btnO = new JButton("O");
		btnO.setBackground(UIManager.getColor("Button.background"));
		btnO.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnO.setEnabled(false);
		GridBagConstraints gbc_btnO = new GridBagConstraints();
		gbc_btnO.fill = GridBagConstraints.BOTH;
		gbc_btnO.insets = new Insets(0, 0, 5, 5);
		gbc_btnO.gridx = 9;
		gbc_btnO.gridy = 1;
		Teclado.add(btnO, gbc_btnO);

		JButton btnP = new JButton("P");
		btnP.setBackground(UIManager.getColor("Button.background"));
		btnP.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnP.setEnabled(false);
		GridBagConstraints gbc_btnP = new GridBagConstraints();
		gbc_btnP.fill = GridBagConstraints.BOTH;
		gbc_btnP.insets = new Insets(0, 0, 5, 0);
		gbc_btnP.gridx = 10;
		gbc_btnP.gridy = 1;
		Teclado.add(btnP, gbc_btnP);

		JButton btnA = new JButton("A");
		btnA.setBackground(UIManager.getColor("Button.background"));
		btnA.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnA.setEnabled(false);
		GridBagConstraints gbc_btnA = new GridBagConstraints();
		gbc_btnA.fill = GridBagConstraints.BOTH;
		gbc_btnA.insets = new Insets(0, 0, 5, 5);
		gbc_btnA.gridx = 1;
		gbc_btnA.gridy = 2;
		Teclado.add(btnA, gbc_btnA);

		JButton btnS = new JButton("S");
		btnS.setBackground(UIManager.getColor("Button.background"));
		btnS.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnS.setEnabled(false);
		GridBagConstraints gbc_btnS = new GridBagConstraints();
		gbc_btnS.fill = GridBagConstraints.BOTH;
		gbc_btnS.insets = new Insets(0, 0, 5, 5);
		gbc_btnS.gridx = 2;
		gbc_btnS.gridy = 2;
		Teclado.add(btnS, gbc_btnS);

		JButton btnD = new JButton("D");
		btnD.setBackground(UIManager.getColor("Button.background"));
		btnD.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnD.setEnabled(false);
		GridBagConstraints gbc_btnD = new GridBagConstraints();
		gbc_btnD.fill = GridBagConstraints.BOTH;
		gbc_btnD.insets = new Insets(0, 0, 5, 5);
		gbc_btnD.gridx = 3;
		gbc_btnD.gridy = 2;
		Teclado.add(btnD, gbc_btnD);

		JButton btnF = new JButton("F");
		btnF.setBackground(UIManager.getColor("Button.background"));
		btnF.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnF.setEnabled(false);
		GridBagConstraints gbc_btnF = new GridBagConstraints();
		gbc_btnF.fill = GridBagConstraints.BOTH;
		gbc_btnF.insets = new Insets(0, 0, 5, 5);
		gbc_btnF.gridx = 4;
		gbc_btnF.gridy = 2;
		Teclado.add(btnF, gbc_btnF);

		JButton btnG = new JButton("G");
		btnG.setBackground(UIManager.getColor("Button.background"));
		btnG.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnG.setEnabled(false);
		GridBagConstraints gbc_btnG = new GridBagConstraints();
		gbc_btnG.fill = GridBagConstraints.BOTH;
		gbc_btnG.insets = new Insets(0, 0, 5, 5);
		gbc_btnG.gridx = 5;
		gbc_btnG.gridy = 2;
		Teclado.add(btnG, gbc_btnG);

		JButton btnH = new JButton("H");
		btnH.setBackground(UIManager.getColor("Button.background"));
		btnH.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnH.setEnabled(false);
		GridBagConstraints gbc_btnH = new GridBagConstraints();
		gbc_btnH.fill = GridBagConstraints.BOTH;
		gbc_btnH.insets = new Insets(0, 0, 5, 5);
		gbc_btnH.gridx = 6;
		gbc_btnH.gridy = 2;
		Teclado.add(btnH, gbc_btnH);

		JButton btnJ = new JButton("J");
		btnJ.setBackground(UIManager.getColor("Button.background"));
		btnJ.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnJ.setEnabled(false);
		GridBagConstraints gbc_btnJ = new GridBagConstraints();
		gbc_btnJ.fill = GridBagConstraints.BOTH;
		gbc_btnJ.insets = new Insets(0, 0, 5, 5);
		gbc_btnJ.gridx = 7;
		gbc_btnJ.gridy = 2;
		Teclado.add(btnJ, gbc_btnJ);

		JButton btnK = new JButton("K");
		btnK.setBackground(UIManager.getColor("Button.background"));
		btnK.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnK.setEnabled(false);
		GridBagConstraints gbc_btnK = new GridBagConstraints();
		gbc_btnK.fill = GridBagConstraints.BOTH;
		gbc_btnK.insets = new Insets(0, 0, 5, 5);
		gbc_btnK.gridx = 8;
		gbc_btnK.gridy = 2;
		Teclado.add(btnK, gbc_btnK);

		JButton btnL = new JButton("L");
		btnL.setBackground(UIManager.getColor("Button.background"));
		btnL.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnL.setEnabled(false);
		GridBagConstraints gbc_btnL = new GridBagConstraints();
		gbc_btnL.fill = GridBagConstraints.BOTH;
		gbc_btnL.insets = new Insets(0, 0, 5, 5);
		gbc_btnL.gridx = 9;
		gbc_btnL.gridy = 2;
		Teclado.add(btnL, gbc_btnL);

		JButton btn� = new JButton("\u00D1");
		btn�.setBackground(UIManager.getColor("Button.background"));
		btn�.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btn�.setEnabled(false);
		GridBagConstraints gbc_btn� = new GridBagConstraints();
		gbc_btn�.fill = GridBagConstraints.BOTH;
		gbc_btn�.insets = new Insets(0, 0, 5, 0);
		gbc_btn�.gridx = 10;
		gbc_btn�.gridy = 2;
		Teclado.add(btn�, gbc_btn�);

		JButton btnShift = new JButton("^");
		btnShift.setBackground(UIManager.getColor("Button.background"));
		btnShift.setFont(new Font("Tahoma", Font.BOLD, 15));
		btnShift.setEnabled(false);
		GridBagConstraints gbc_btnShift = new GridBagConstraints();
		gbc_btnShift.fill = GridBagConstraints.BOTH;
		gbc_btnShift.insets = new Insets(0, 0, 5, 5);
		gbc_btnShift.gridx = 0;
		gbc_btnShift.gridy = 3;
		Teclado.add(btnShift, gbc_btnShift);

		JButton btnZ = new JButton("Z");
		btnZ.setBackground(UIManager.getColor("Button.background"));
		btnZ.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnZ.setEnabled(false);
		GridBagConstraints gbc_btnZ = new GridBagConstraints();
		gbc_btnZ.fill = GridBagConstraints.BOTH;
		gbc_btnZ.insets = new Insets(0, 0, 5, 5);
		gbc_btnZ.gridx = 1;
		gbc_btnZ.gridy = 3;
		Teclado.add(btnZ, gbc_btnZ);

		JButton btnX = new JButton("X");
		btnX.setBackground(UIManager.getColor("Button.background"));
		btnX.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnX.setEnabled(false);
		GridBagConstraints gbc_btnX = new GridBagConstraints();
		gbc_btnX.fill = GridBagConstraints.BOTH;
		gbc_btnX.insets = new Insets(0, 0, 5, 5);
		gbc_btnX.gridx = 2;
		gbc_btnX.gridy = 3;
		Teclado.add(btnX, gbc_btnX);

		JButton btnC = new JButton("C");
		btnC.setBackground(UIManager.getColor("Button.background"));
		btnC.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnC.setEnabled(false);
		GridBagConstraints gbc_btnC = new GridBagConstraints();
		gbc_btnC.fill = GridBagConstraints.BOTH;
		gbc_btnC.insets = new Insets(0, 0, 5, 5);
		gbc_btnC.gridx = 3;
		gbc_btnC.gridy = 3;
		Teclado.add(btnC, gbc_btnC);

		JButton btnV = new JButton("V");
		btnV.setBackground(UIManager.getColor("Button.background"));
		btnV.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnV.setEnabled(false);
		GridBagConstraints gbc_btnV = new GridBagConstraints();
		gbc_btnV.fill = GridBagConstraints.BOTH;
		gbc_btnV.insets = new Insets(0, 0, 5, 5);
		gbc_btnV.gridx = 4;
		gbc_btnV.gridy = 3;
		Teclado.add(btnV, gbc_btnV);

		JButton btnB = new JButton("B");
		btnB.setBackground(UIManager.getColor("Button.background"));
		btnB.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnB.setEnabled(false);
		GridBagConstraints gbc_btnB = new GridBagConstraints();
		gbc_btnB.fill = GridBagConstraints.BOTH;
		gbc_btnB.insets = new Insets(0, 0, 5, 5);
		gbc_btnB.gridx = 5;
		gbc_btnB.gridy = 3;
		Teclado.add(btnB, gbc_btnB);

		JButton btnN = new JButton("N");
		btnN.setBackground(UIManager.getColor("Button.background"));
		btnN.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnN.setEnabled(false);
		GridBagConstraints gbc_btnN = new GridBagConstraints();
		gbc_btnN.fill = GridBagConstraints.BOTH;
		gbc_btnN.insets = new Insets(0, 0, 5, 5);
		gbc_btnN.gridx = 6;
		gbc_btnN.gridy = 3;
		Teclado.add(btnN, gbc_btnN);

		JButton btnM = new JButton("M");
		btnM.setBackground(UIManager.getColor("Button.background"));
		btnM.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnM.setEnabled(false);
		GridBagConstraints gbc_btnM = new GridBagConstraints();
		gbc_btnM.fill = GridBagConstraints.BOTH;
		gbc_btnM.insets = new Insets(0, 0, 5, 5);
		gbc_btnM.gridx = 7;
		gbc_btnM.gridy = 3;
		Teclado.add(btnM, gbc_btnM);

		JButton btnComa = new JButton(",");
		btnComa.setBackground(UIManager.getColor("Button.background"));
		btnComa.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnComa.setEnabled(false);
		GridBagConstraints gbc_btnComa = new GridBagConstraints();
		gbc_btnComa.fill = GridBagConstraints.BOTH;
		gbc_btnComa.insets = new Insets(0, 0, 5, 5);
		gbc_btnComa.gridx = 8;
		gbc_btnComa.gridy = 3;
		Teclado.add(btnComa, gbc_btnComa);

		JButton btnPunto = new JButton(".");
		btnPunto.setBackground(UIManager.getColor("Button.background"));
		btnPunto.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnPunto.setEnabled(false);
		GridBagConstraints gbc_btnPunto = new GridBagConstraints();
		gbc_btnPunto.fill = GridBagConstraints.BOTH;
		gbc_btnPunto.insets = new Insets(0, 0, 5, 5);
		gbc_btnPunto.gridx = 9;
		gbc_btnPunto.gridy = 3;
		Teclado.add(btnPunto, gbc_btnPunto);

		JButton btnSpace = new JButton("");
		btnSpace.setFont(new Font("Arial Rounded MT Bold", Font.BOLD, 15));
		btnSpace.setBackground(UIManager.getColor("Button.background"));
		btnSpace.setEnabled(false);
		GridBagConstraints gbc_btnSpace = new GridBagConstraints();
		gbc_btnSpace.fill = GridBagConstraints.BOTH;
		gbc_btnSpace.gridwidth = 6;
		gbc_btnSpace.insets = new Insets(0, 0, 0, 5);
		gbc_btnSpace.gridx = 2;
		gbc_btnSpace.gridy = 4;
		Teclado.add(btnSpace, gbc_btnSpace);

		JPanel Estadisticas = new JPanel();
		Center.add(Estadisticas, "cell 1 0,grow");
		Estadisticas.setBorder(null);
		Estadisticas.setBackground(new Color(139, 69, 19));
		Estadisticas.setLayout(new GridLayout(4, 1, 0, 0));

		JPanel pulsacionesT = new JPanel();
		pulsacionesT.setBackground(new Color(139, 69, 19));
		pulsacionesT.setBorder(new LineBorder(new Color(160, 82, 45), 2, true));
		Estadisticas.add(pulsacionesT);
		pulsacionesT.setLayout(new GridLayout(0, 1, 0, 0));

		JTextField txtPulsacionesTotales = new JTextField();
		txtPulsacionesTotales.setEditable(false);
		txtPulsacionesTotales.setHorizontalAlignment(SwingConstants.CENTER);
		txtPulsacionesTotales.setBorder(null);
		txtPulsacionesTotales.setBackground(new Color(139, 69, 19));
		txtPulsacionesTotales.setForeground(new Color(218, 165, 32));
		txtPulsacionesTotales.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		txtPulsacionesTotales.setText("Pulsaciones Totales");
		pulsacionesT.add(txtPulsacionesTotales);
		txtPulsacionesTotales.setColumns(10);

		numPT = new JTextField();
		numPT.setEditable(false);
		numPT.setBackground(new Color(238, 232, 170));
		numPT.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		numPT.setForeground(Color.BLUE);
		numPT.setHorizontalAlignment(SwingConstants.CENTER);
		numPT.setColumns(1);
		pulsacionesT.add(numPT);

		JPanel pulsacionesMin = new JPanel();
		pulsacionesMin.setBorder(new LineBorder(new Color(160, 82, 45), 2, true));
		pulsacionesMin.setBackground(new Color(139, 69, 19));
		Estadisticas.add(pulsacionesMin);
		pulsacionesMin.setLayout(new GridLayout(0, 1, 0, 0));

		JTextField txtPulsacionesminuto = new JTextField();
		txtPulsacionesminuto.setEditable(false);
		txtPulsacionesminuto.setText("Pulsaciones/Minuto");
		txtPulsacionesminuto.setHorizontalAlignment(SwingConstants.CENTER);
		txtPulsacionesminuto.setForeground(new Color(218, 165, 32));
		txtPulsacionesminuto.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		txtPulsacionesminuto.setColumns(10);
		txtPulsacionesminuto.setBorder(null);
		txtPulsacionesminuto.setBackground(new Color(139, 69, 19));
		pulsacionesMin.add(txtPulsacionesminuto);

		numPM = new JTextField();
		numPM.setEditable(false);
		numPM.setBackground(new Color(238, 232, 170));
		numPM.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		numPM.setForeground(Color.BLUE);
		numPM.setHorizontalAlignment(SwingConstants.CENTER);
		numPM.setColumns(1);
		pulsacionesMin.add(numPM);

		JPanel errores = new JPanel();
		errores.setBorder(new LineBorder(new Color(160, 82, 45), 2, true));
		errores.setBackground(new Color(139, 69, 19));
		Estadisticas.add(errores);
		errores.setLayout(new GridLayout(0, 1, 0, 0));

		JTextField txtErrores = new JTextField();
		txtErrores.setEditable(false);
		txtErrores.setText("Errores");
		txtErrores.setHorizontalAlignment(SwingConstants.CENTER);
		txtErrores.setForeground(new Color(218, 165, 32));
		txtErrores.setFont(new Font("Times New Roman", Font.PLAIN, 23));
		txtErrores.setColumns(10);
		txtErrores.setBorder(null);
		txtErrores.setBackground(new Color(139, 69, 19));
		errores.add(txtErrores);

		numErrores = new JTextField();
		numErrores.setEditable(false);
		numErrores.setText(Integer.toString(estadisticas.getErrores()) + " ( "
				+ Double.toString(estadisticas.getPorcentajeErrores(fichero.getLongitud())) + "% )");
		numErrores.setBackground(new Color(238, 232, 170));
		numErrores.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		numErrores.setForeground(Color.RED);
		numErrores.setHorizontalAlignment(SwingConstants.CENTER);
		numErrores.setColumns(10);
		errores.add(numErrores);

		JPanel tiempo = new JPanel();
		tiempo.setBorder(new LineBorder(new Color(160, 82, 45), 2, true));
		tiempo.setBackground(new Color(139, 69, 19));
		Estadisticas.add(tiempo);
		tiempo.setLayout(new GridLayout(0, 1, 0, 0));

		JTextField txtTiempo = new JTextField();
		txtTiempo.setEditable(false);
		txtTiempo.setText("Tiempo");
		txtTiempo.setHorizontalAlignment(SwingConstants.CENTER);
		txtTiempo.setForeground(new Color(218, 165, 32));
		txtTiempo.setFont(new Font("Times New Roman", Font.PLAIN, 23));
		txtTiempo.setColumns(10);
		txtTiempo.setBorder(null);
		txtTiempo.setBackground(new Color(139, 69, 19));
		tiempo.add(txtTiempo);

		numTiempo = new JTextField();
		numTiempo.setEditable(false);
		numTiempo.setText(estadisticas.getTiempo());
		numTiempo.setHorizontalAlignment(SwingConstants.CENTER);
		numTiempo.setForeground(new Color(0, 100, 0));
		numTiempo.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		numTiempo.setColumns(10);
		numTiempo.setBackground(new Color(238, 232, 170));
		tiempo.add(numTiempo);

		JPanel Bottom = new JPanel();
		Bottom.setBackground(new Color(139, 69, 19));
		frmPP.getContentPane().add(Bottom, "cell 1 2,alignx center,aligny center");
		Bottom.setLayout(new MigLayout("", "[50%,grow][50%,grow]", "[164px]"));

		JPanel panel = new JPanel();
		Bottom.add(panel, "cell 1 0,grow");
		panel.setBorder(new LineBorder(new Color(160, 82, 45), 3, true));
		panel.setBackground(new Color(139, 69, 19));
		panel.setLayout(new MigLayout("", "[65%,grow][15%,grow,fill][-38.00][5%,grow,fill][15%,grow,fill]", "[10%,grow][90%,grow]"));

		JButton btnVolver = new JButton("VOLVER");
		panel.add(btnVolver, "cell 4 0,grow");
		btnVolver.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		btnVolver.setEnabled(false);
		
		/* EVENTOS */
		// BTN Vuelve a la pantalla inicial
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "�Quieres salir de la lecci�n?", "Salir", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					Fichero.guardarEstFichero(estadisticas);
					frmPP.setVisible(false);
					menu.frmMenu.setVisible(true);
					btnVolver.setEnabled(false);
				}
			}
		});

		// BTN Salir
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "�Quieres salir de la lecci�n?", "Salir", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					timer.stop();
					frmPP.dispose();
					System.exit(0);
				}
			}
		});

		// BTN Accerca de
		btnAcercaDe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(null,
						"Creador:     Jorge Loarte Smith\nFecha:         15/10/2019\nVersi�n:      1.0.0", "Acerca de",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});

		// Cojer pulsaciones
		frmPP.addKeyListener(new KeyAdapter() {
			@Override
			// Evento para cojer la tecla cuando se levanta
			public void keyReleased(KeyEvent e) {

				if (usoTeclado) {
					// Cambiar color a los botones
					if (e.getKeyCode() == e.VK_SHIFT) {
						if (e.getKeyCode() == e.VK_SHIFT)
							btnShift.setBackground(normalColor);
						return;
					}
					
					if ( !compruebaBtn(Teclado, e.getKeyChar()) )
						return;

					cambiaColorBtn(normalColor, Teclado, e.getKeyChar());

					// Suma una pulsacion
					estadisticas.sumaPulsaciones();
					numPT.setText( Integer.toString(estadisticas.getPulsacionesT()) );

					// Condicion de entrada correcta de tecla
					if (leccion[posicionCursor] == e.getKeyChar()) {
						error = false;
					} else {
						estadisticas.sumaError();
						error = true;
						numErrores.setText(Integer.toString(estadisticas.getErrores()) + " ( "
								+ Double.toString(estadisticas.getPorcentajeErrores(fichero.getLongitud())) + "% )");
					}

					subrayado(texto);
					posicionCursor++;
					error = false;

					// Cuando pulsas el primer boton empieza el tiempo
					if (!timer.isRunning())
						timer.start();

					// Termina el tiempo y desabilita el teclado para no poder ser pulsado
					if (posicionCursor == fichero.getLongitud()) {
						btnVolver.setEnabled(true);
						numPM.setText(estadisticas.getPulsacionesM());
						for (int i = 0; i < Teclado.getComponentCount(); i++)
							Teclado.getComponent(i).setBackground(normalColor);
						timer.stop();
						usoTeclado = false;
					}
				}
			}

			// Evento cuando se presiona la tecla
			public void keyPressed(KeyEvent e) {
				if (usoTeclado) {
					if (e.getKeyCode() == e.VK_SHIFT) {
						btnShift.setBackground(pressColor);
						return;
					}
					
					if ( !compruebaBtn(Teclado, e.getKeyChar()) )
						return;

					cambiaColorBtn(pressColor, Teclado, e.getKeyChar());
				}
			}
		});

		// Tiempo
		timer = new Timer(1000, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				estadisticas.sumaTiempo();
				numTiempo.setText(estadisticas.getTiempo());
				if (estadisticas.tiempo == 10000)
					timer.stop();
				if (estadisticas.getSeg() != 0) {
					numPM.setText(estadisticas.getPulsacionesM());
				}
			}
		});
	}

	// Subraya cada letra dependiendo del color
	private void subrayado(JTextArea t) {
		try {
			if (error)
				t.getHighlighter().addHighlight(posicionCursor, posicionCursor + 1, hlbad);
			else
				t.getHighlighter().addHighlight(posicionCursor, posicionCursor + 1, hlgood);
		} catch (BadLocationException e) {
			System.out.println("El subrayado esta funcionando mal: " + e.getMessage());
		}
	}

	// Cambia el color de los botones pulsados
	private void cambiaColorBtn(Color c, JPanel Teclado, char key) {
		for (int i = 0; i < Teclado.getComponentCount(); i++) {
			JButton btn = (JButton) Teclado.getComponent(i);
			if (btn.getText().toLowerCase().indexOf(key) != -1) {
				Teclado.getComponent(i).setBackground(c);
				return;
			}
		}
		if (key == ' ')
			Teclado.getComponent(40).setBackground(c);
	}
	
	// Comprueba si se ha pulsado un boton correctamente
	private boolean compruebaBtn (JPanel Teclado, char key) {
		for (int i = 0; i < Teclado.getComponentCount(); i++) {
			JButton btn = (JButton) Teclado.getComponent(i);
			if (btn.getText().toLowerCase().indexOf(key) != -1)
				return true;
		}
		if (key == ' ')
			return true;
		return false;
	} 

	public void reseteoPP () {
		usoTeclado = true;
		posicionCursor = 0;
		estadisticas = new Estadisticas();
		numPT.setText( Integer.toString(estadisticas.getPulsacionesT()) );
		numPM.setText( estadisticas.getPulsacionesM() );
		numErrores.setText(Integer.toString(estadisticas.getErrores()) + " ( "
				+ Double.toString(estadisticas.getPorcentajeErrores(fichero.getLongitud())) + "% )");
		numTiempo.setText(estadisticas.getTiempo());
	}
}