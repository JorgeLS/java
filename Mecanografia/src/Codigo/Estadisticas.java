package Codigo;

public class Estadisticas {
	
	protected int pulsacionesT;
	protected double pulsacionesM;
	protected int errores;
	protected int tiempo;
	protected int hora;
	protected int min;
	public String[] nomEstadisticas =
	{
		"fc=",
		"pt=",
		"pm=",
		"er=",
		"tm="
	};
	
	public Estadisticas() {
		this.pulsacionesT = 0;
		this.pulsacionesM = 0;
		this.errores = 0;
		this.tiempo = 0;
		this.hora = 0;
		this.min = 0;
	}

	public int getPulsacionesT() {
		return pulsacionesT;
	}
	public void setPulsacionesT(int pulsacionesT) {
		this.pulsacionesT = pulsacionesT;
	}
	public void sumaPulsaciones() {
		this.pulsacionesT++;
	}

	public String getPulsacionesM() {
		if (this.tiempo == 0)
			this.pulsacionesM = 0;
		else
			this.pulsacionesM = this.pulsacionesT * (60 / this.tiempo);
		
		return String.format("%.0f", this.pulsacionesM);
	}

	public int getErrores() {
		return errores;
	}
	public void sumaError () {
		this.errores++;
	}
	public double getPorcentajeErrores(int longitud) {
		if (longitud == 0)
			return 0;
		return (errores * 100 / longitud);
	}
	
	public String getTiempo() {
		if (this.tiempo % 60 == 0 && this.tiempo != 0)
			min++;
		
		if (this.tiempo % 3600 == 0 && this.tiempo != 0)
			hora++;
		
		return String.format("%02d", this.hora) + ":" + String.format("%02d", this.min % 60) + ":" + String.format("%02d", this.tiempo % 60);
	}
	public void sumaTiempo() {
		this.tiempo++;
	}
	public int getSeg() {
		return tiempo;
	}
	
	// Devuelve las estadisticas de una leccion y usuario en concreto
	public String[] getEstadisticasFichero(String user, int leccion) {
		String fichero;
		String separador;
		int separadorLugar;
		
		fichero = Fichero.getUsuarioEst(user);
		
		String[] estadisticas = new String[nomEstadisticas.length];
		
		for (int i = 0; i < nomEstadisticas.length; i++) {
			separador = "/%" + leccion + "/ " + nomEstadisticas[i];
			separadorLugar = fichero.indexOf(separador);
			estadisticas[i] = fichero.substring(separadorLugar + separador.length(),  separadorLugar + fichero.substring(separadorLugar).indexOf(" -"));
		}
		return estadisticas;
	}
}