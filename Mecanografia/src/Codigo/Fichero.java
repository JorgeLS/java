package Codigo;

import java.io.*;
import java.time.*;
import java.time.format.*;

public class Fichero {

	protected String fileName;
	protected int longitud;
	protected int leccion;

	public Fichero(String fileName, int leccion) {
		this.fileName = fileName;
		this.longitud = getLeccion().length();
		this.leccion = leccion;
	}

	public int getLongitud() {
		return longitud;
	}

	public void setLongitud() {
		this.longitud = getLeccion().length();
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	// Devuelve la leccion completa
	public String getLeccion() {
		String textoFichero = "";
		String txt = "";
		try {
			File fichero = new File(this.fileName);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);

			while ( (txt = br.readLine()) != null) {
				if ( txt.indexOf("#"+ leccion +"- ") != -1)
					textoFichero = txt.substring(4);
			}

			br.close();
		} catch (IOException e) {
			System.out.println("No se ha encontrado el archivo de las lecciones: " + e.getMessage());
		}
		return textoFichero;
	}
	public void setLeccion(int leccion) {
		this.leccion = leccion;
	}

	// Comprueba si usuarios
	public boolean comprobarUsuarios(String usr, String pass) {
		String txt = "";

		if (usr.equals("") || pass.equals(""))
			return false;

		try {
			File fichero = new File(this.fileName);

			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);

			while (txt != null) {
				if ((txt = br.readLine()) != null)
					if (txt.indexOf("#-/ ") != -1)
						if (txt.substring(4).indexOf(usr + " ,-,") != -1
								&& txt.substring(4).indexOf(",-, " + pass + ";") != -1) {
							br.close();
							return true;
						}
			}

			br.close();
		} catch (IOException e) {
			System.out.println("No se ha encontrado el archivo de los usuarios: " + e.getMessage());
		}
		return false;
	}

	// Devuelve solo las estadisticas de un usuario
	public static String getUsuarioEst(String user) {
		String txt = "";
		String txtFichero = "";

		try {
			File fichero = new File(".//src//Media//estadisticas.txt");

			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);

			while ((txt = br.readLine()) != null) {
				if (txt.indexOf("## " + user) != -1) {
					while ((txt = br.readLine()) != null) {
						if (txt.indexOf("##") != -1)
							break;
						txtFichero += txt + "\n";
					}
				}
			}

			br.close();
			return txtFichero;
		} catch (IOException e) {
			System.out.println("No se ha encontrado el archivo de los usuarios: " + e.getMessage());
		}

		return null;
	}

	public static boolean comprobarFicheros(String fileName) {

		try {
			String txt = "";
			
			File fichero = new File(fileName);
			FileReader fr = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fr);
			
			// Si no hay nada en el fichero da error
			if ((txt = br.readLine()) == null) {
				br.close();
				return false;
			}

			br.close();
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	// Guarda las estadisticas de un usuario y ficehro en concreto
	public static void guardarEstFichero(Estadisticas est) {

		LocalDateTime date = LocalDateTime.now();

		String fichero = "";
		int letra;

		String[] estadisticas =
		{
				date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")),
				Integer.toString(est.getPulsacionesT()),
				est.getPulsacionesM(),
				Integer.toString(est.getErrores()),
				est.getTiempo()
		};

		try {
			File file = new File(".//src//Media//estadisticas.txt");
			FileReader fr = new FileReader(file);

			while ((letra = fr.read()) != -1) {
				fichero += (char) letra;
			}

			fr.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

		int posUsuario = fichero.indexOf("## " + PantallaInicial.usuario);

		for (int i = 0; i < est.nomEstadisticas.length; i++) {
			String separador = "/%" + PantallaInicial.leccion + "/ " + est.nomEstadisticas[i];
			int posSeparador = fichero.indexOf(separador);

			String cambiador = fichero.substring (
					posUsuario + posSeparador + separador.length(),
					posUsuario + posSeparador + fichero.substring(posUsuario + posSeparador).indexOf(" -")
					);
			
			cambiador = cambiador.replace("=", "");
			fichero = fichero.replace(
					"/%" + PantallaInicial.leccion + "/ " + est.nomEstadisticas[i] + "" + cambiador + " -",
					"/%" + PantallaInicial.leccion + "/ " + est.nomEstadisticas[i] + "" + estadisticas[i] + " -");
		}

		try {
			File file = new File(".//src//Media//estadisticas.txt");
			FileWriter fw = new FileWriter(file);

			fw.write(fichero);

			fw.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
}
