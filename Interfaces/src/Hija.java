import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Hija {

	public JFrame frame;
	static Padre padre;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Hija window = new Hija(padre);
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Hija(Padre padre) {
		initialize();
		this.padre = padre;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton btnIrAlPadre = new JButton("Ir al Padre -->");
		btnIrAlPadre.setFont(new Font("Tahoma", Font.PLAIN, 16));
		frame.getContentPane().add(btnIrAlPadre, BorderLayout.CENTER);
		

		btnIrAlPadre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				padre.frame.setVisible(true);
			}
		});
	}
}
