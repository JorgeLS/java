import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Color;
import javax.swing.JCheckBox;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.border.LineBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.Rectangle;
import java.awt.Point;
import com.toedter.calendar.JCalendar;
import javax.swing.ButtonGroup;

public class Ejercicio1 {

	public JFrame frmEjercicio;
	private JPasswordField passfield;
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio1 window = new Ejercicio1();
					window.frmEjercicio.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmEjercicio = new JFrame();
		frmEjercicio.setResizable(false);
		frmEjercicio.setTitle("Ejercicio1");
		frmEjercicio.setBounds(100, 100, 682, 445);
		frmEjercicio.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmEjercicio.getContentPane().setLayout(null);
		
		JRadioButton rdbtn1 = new JRadioButton("Radio");
		buttonGroup.add(rdbtn1);
		rdbtn1.setBounds(10, 51, 109, 23);
		frmEjercicio.getContentPane().add(rdbtn1);
		
		JRadioButton rdbtn2 = new JRadioButton("Radio");
		buttonGroup.add(rdbtn2);
		rdbtn2.setBounds(10, 77, 109, 23);
		frmEjercicio.getContentPane().add(rdbtn2);
		
		JRadioButton rdbtn3 = new JRadioButton("Radio");
		buttonGroup.add(rdbtn3);
		rdbtn3.setBounds(10, 103, 109, 23);
		frmEjercicio.getContentPane().add(rdbtn3);
		
		JLabel lbl = new JLabel("Label");
		lbl.setBackground(Color.BLUE);
		lbl.setForeground(Color.BLACK);
		lbl.setHorizontalAlignment(SwingConstants.CENTER);
		lbl.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lbl.setBounds(10, 11, 109, 28);
		frmEjercicio.getContentPane().add(lbl);
		
		JCheckBox chckbtn = new JCheckBox("CheckButton");
		chckbtn.setBounds(10, 136, 97, 23);
		frmEjercicio.getContentPane().add(chckbtn);
		
		passfield = new JPasswordField();
		passfield.setBounds(10, 176, 123, 23);
		frmEjercicio.getContentPane().add(passfield);
		
		JTextArea txtarea = new JTextArea();
		txtarea.setLineWrap(true);
		txtarea.setBorder(new LineBorder(new Color(0, 0, 0)));
		txtarea.setText("Soy un area de texto");
		txtarea.setBounds(125, 11, 330, 100);
		frmEjercicio.getContentPane().add(txtarea);
		
		JButton btn = new JButton("Bot\u00F3n");
		btn.setFont(new Font("Segoe WP Semibold", Font.PLAIN, 17));
		btn.setForeground(Color.RED);
		btn.setBounds(217, 136, 169, 41);
		frmEjercicio.getContentPane().add(btn);
		
		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setIcon(new ImageIcon("C:\\Users\\DAM\\Downloads\\mono.jpg"));
		label.setBounds(10, 210, 646, 185);
		frmEjercicio.getContentPane().add(label);
		
		JCalendar calendar = new JCalendar();
		calendar.setBounds(482, 11, 184, 153);
		frmEjercicio.getContentPane().add(calendar);
	}
}
