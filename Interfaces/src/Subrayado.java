import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JTextArea;

public class Subrayado {

	private JFrame frame;
	public int cursor = 0;
	public boolean error = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Subrayado window = new Subrayado();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Subrayado() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 727, 479);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setFocusable(true);

		JTextPane txt = new JTextPane();
		txt.setEditable(false);
		txt.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		txt.setText(
				"La lecjasdoasj asdj asdk askdn jkasnjd bajksdb jkasbdj bajksdbk jabsdj bajksd bajksdb ajksbd jabsdjk bajksdb jkabsd jka");
		txt.setBounds(77, 67, 537, 146);
		frame.getContentPane().add(txt);

		JTextArea txtA = new JTextArea();
		txtA.setWrapStyleWord(true);
		txtA.setLineWrap(true);
		txtA.setText(
				"La lecjasdoasj asdj asdk askdn jkasnjd bajksdb jkasbdj bajksdbk jabsdj bajksd bajksdb ajksbd jabsdjk bajksdb jkabsd jka");
		txtA.setEditable(false);
		txtA.setBounds(77, 223, 537, 167);
		frame.getContentPane().add(txtA);

		// Eventos
		frame.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent e) {

				if (e.getKeyChar() == 'm') {
					cursor++;
					error = false;
				}
				if (e.getKeyChar() == 'e')
					error = !error;
				//System.out.println(e.getKeyChar() + " .-. " + cursor + " -.- " + error);

				subrayado(txt);
				subrayadoBien(txtA);

			}
		});
	}

	public void subrayado(JTextPane t) {
		DefaultHighlighter.DefaultHighlightPainter hlPgood = new DefaultHighlighter.DefaultHighlightPainter(
				Color.GREEN);
		DefaultHighlighter.DefaultHighlightPainter hlPbad = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);
		t.getHighlighter().removeAllHighlights();
		try {
			t.getHighlighter().addHighlight(0, cursor, hlPgood);
			if (error) {
				t.getHighlighter().addHighlight(cursor, cursor + 1, hlPbad);
			}
		} catch (BadLocationException e) {
			System.out.println("Mal");
		}
	}

	public void subrayadoBien(JTextArea t) {
		DefaultHighlighter.DefaultHighlightPainter hlPgood = new DefaultHighlighter.DefaultHighlightPainter(
				Color.GREEN);
		DefaultHighlighter.DefaultHighlightPainter hlPbad = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);
		try {
			t.getHighlighter().addHighlight(0, cursor, hlPgood);
			if (error) {
				t.getHighlighter().addHighlight(cursor, cursor + 1, hlPbad);
			}
		} catch (BadLocationException e) {
			System.out.println("Mal");
		}
	}
}
