import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Login {

	public JFrame frmLogin;
	public JFrame frmEjercicio;
	private JPasswordField passwordField;
	private JTextField userField;
	
	private String user = "JorgeLS";
	private String password = "1234";

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	private void initialize() {
		
		frmLogin = new JFrame();
		frmLogin.setResizable(false);
		frmLogin.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 13));
		frmLogin.setTitle("Login");
		frmLogin.setBounds(100, 100, 700, 400);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario:");
		lblUsuario.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblUsuario.setBounds(250, 75, 129, 14);
		frmLogin.getContentPane().add(lblUsuario);
		
		userField = new JTextField();
		userField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		userField.setBounds(250, 100, 129, 20);
		frmLogin.getContentPane().add(userField);
		userField.setColumns(10);
		
		JLabel lblPassword = new JLabel("Contrase\u00F1a:");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblPassword.setBounds(250, 144, 129, 14);
		frmLogin.getContentPane().add(lblPassword);
		
		passwordField = new JPasswordField();
		passwordField.setVerifyInputWhenFocusTarget(false);
		passwordField.setFont(new Font("Tahoma", Font.PLAIN, 13));
		passwordField.setBounds(250, 169, 129, 20);
		frmLogin.getContentPane().add(passwordField);
		
		JButton btnIniciarSesin = new JButton("Iniciar Sesi\u00F3n");
		btnIniciarSesin.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnIniciarSesin.setBounds(250, 225, 129, 34);
		frmLogin.getContentPane().add(btnIniciarSesin);
		
		
		// Eventos
		btnIniciarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (userField.getText().equals(user)) {
					if (passwordField.getText().equals(password)) {
						JOptionPane.showMessageDialog(null, "Has podido entrar");
						
					}
					else
						JOptionPane.showMessageDialog(null, "La contrase�a es inv�lida");						
				}
				else
					JOptionPane.showMessageDialog(null, "El usuario es inv�lido");					
			}
		});
	}
}
