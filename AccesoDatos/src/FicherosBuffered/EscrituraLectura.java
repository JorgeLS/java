package FicherosBuffered;

public class EscrituraLectura {

	public static void main(String[] args) {

		Fichero fichero = new Fichero(".//src//txtFicheros//nombres.txt");
		
		String[] tablaEscritura = {"Juan", "Ana", "Pedro", "Manolo"};
		String[] tablaLectura = new String[10];
		
		fichero.escribirTabla(tablaEscritura);
		
		fichero.leerFichero(tablaLectura);
		
		for (int i = 0; i < tablaLectura.length; i++)
			if (tablaLectura[i] != null)
				System.out.println(tablaLectura[i]);
		
		/////////////////////////////////////////////////////
		System.out.println("------------------------------");
		/////////////////////////////////////////////////////
		
		Persona[] personas = new Persona[4];
		
		personas[0] = new Persona("Juan", 15);
		personas[1] = new Persona("Ana", 10);
		personas[2] = new Persona("Baura", 13);
		personas[3] = new Persona("Pedro", 32);
		
		fichero.escribirPersonas(personas);
		
		// Borro la tabla para leer desde el fichero 
		for (int i = 0; i < personas.length; i++)
			// Cuando pones un new crea otro espacio en la memoria
			// para Persona pero no elimina el anterior
			personas[i] = new Persona();
		
		fichero.leerPersonas(personas);
		
		// Muestro la tabla
		for (int i = 0; i < personas.length; i++)
			System.out.println(personas[i].toString());
		
		// Nuevo ejercicio llamar y escribir el fichero de forma ordenada
		// Escribir la tabla ordenado por edad y tambien ordenada por nombre Mayor a Menor
		// v1 OrdenarTabla modificandola --> Escribir
		// v2 Escribir ordenadamente sin modificar
		
		System.out.println("Muestra ordenadamente las personas por su edad:");
		fichero.mostrarOrdEdad(personas);
		
		System.out.println("Muestra ordenadamente las personas por su Nombre:");
		fichero.mostrarOrdNombre(personas);
	}
}
