package FicherosBuffered;

import java.io.*;

public class Fichero {
	private String ruta;
	private File fichero;
	
	public Fichero (String ruta) {
		this.ruta = ruta;
		this.fichero = new File(this.ruta);
	}
	
	// Escribir
	public void escribirTabla (String[] tabla) {
		
		try {
			FileWriter fw = new FileWriter (fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for (int i = 0; i < tabla.length; i++) {
				bw.write(tabla[i]);
				bw.newLine();
			}
			
			bw.close();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

	public void escribirPersonas (Persona[] p) {
		
		try {
			FileWriter fw = new FileWriter (fichero);
			BufferedWriter bw = new BufferedWriter(fw);
			
			for (int i = 0; i < p.length; i++) {
				bw.write(p[i].getNombre());
				bw.newLine();
				bw.write(Integer.toString(p[i].getEdad()));
				bw.newLine();
			}
			
			bw.close();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	// Leer
	public void leerFichero (String[] tabla) {

		String linea;
		int pos;
		
		try {
			FileReader fr = new FileReader (fichero);
			BufferedReader br = new BufferedReader(fr);

			linea = "";
			pos = 0;
			
			while ( (linea = br.readLine()) != null)
			{
				tabla[pos++] = linea;
			}
						
			br.close();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public void leerPersonas (Persona[] p) {

		String linea;
		int pos;
		
		try {
			FileReader fr = new FileReader (fichero);
			BufferedReader br = new BufferedReader(fr);

			linea = "";
			pos = 0;
			
			while ( (linea = br.readLine()) != null)
			{
				p[pos].setNombre(linea);
				linea = br.readLine();
				p[pos++].setEdad(Integer.parseInt(linea));
			}
						
			br.close();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}
	
	public void mostrarOrdEdad (Persona[] p) {
		
		//Persona aux;
		int min = 1000;
		int aux = 0;
		
		// Ordena la Persona y luego lo muestra
		/*
		for (int i = 0; i < p.length; i++) {
			for (int j = 0; j < p.length; j++) {
				if (p[i].getEdad() < p[j].getEdad()) {
					aux = p[i];
					p[i] = p[j];
					p[j] = aux;
				}
			}
		}
		
		for (int i = 0; i < p.length; i++)
			System.out.println(p[i].toString());
		*/
		
		for (int i = 0; i < p.length; i++) {
			for (int j = 0; j < p.length; j++) {
				if (p[j].getEdad() < min && p[j].getEdad() > aux)
					min = p[j].getEdad();
			}
			System.out.println(min);
			aux = min;
			min = 1000;
		}
	}
	
	public void mostrarOrdNombre (Persona[] p)
	{
		Persona aux;
		
		for (int i = 0; i < p.length; i++) {
			for (int j = 0; j < p.length; j++) {
				for (int k = 0; k < p[i].getNombre().length() && k < p[j].getNombre().length(); k++) {
					if (p[i].getNombre().charAt(k) < p[j].getNombre().charAt(k)) {
						aux = p[i];
						p[i] = p[j];
						p[j] = aux;
					}
				}
			}
		}
		
		for (int i = 0; i < p.length; i++)
			System.out.println(p[i].toString());
	}
}