package FicherosBuffered;

public class Persona {
	
	private String nombre;
	private int edad;
	
	public Persona () {
		nombre = "";
		edad = -1;
	}
	
	public Persona (String nombre, int edad)
	{
		this.nombre = nombre;
		this.edad = edad;
	}
	
	public String getNombre () {
		return nombre;
	}
	public int getEdad () {
		return edad;
	}
	
	public void setNombre (String nombre) {
		this.nombre = nombre;
	}
	public void setEdad (int edad) {
		this.edad = edad;
	}
	
	public String toString () {
		return nombre + ", tiene " + edad + " a�os.";
	}
}
