package Examen;

public class Empleado {
	
	String nombre;
	int dni;
	
	public Empleado() {
		this.nombre = "";
		this.dni = 0;
	}
	
	public void escribir() {
		System.out.println("El empleado " + nombre + " tiene DNI: " + dni);
	}
	
	public void addNombre (String nombre) {
		this.nombre = nombre;		
	}
	
	public void addDNI (int dni) {
		this.dni = dni;
	}
	
	public String getNombre () {
		return nombre;
	}
	
	public int getDNI () {
		return dni;
	}
}
