package Examen;
import java.io.*;

public class Fichero {

	// Lee un archivo con la estructura -<NEmpleado>-<Nombre>-<DNI>-<Nombre>-<DNI>-... y realiza estas tareas:
	// 		- Leer empleados y guardarlos en un conjunto de datos
	//	 	- Escribir por paltalla los empleados con su DNI
	//		- Escribir por pantalla el nombre del empleado con el DNI m�s Alto
	//		- Escribir por pantalla el nombre del empleado con el DNI m�s Bajo
	public static void main(String[] args) {
		
		//String[] nombreEmp = new String[100];
		//int[] dniEmp = new int[100];
		
		Empleado[] empleados = new Empleado [100];
		// Hay que inicializar la variable
		for (int i = 0; i < empleados.length; i++)
			empleados[i] = new Empleado();
		
		int numEmpleados = 0;
		
		int dniBajo = 0;
		int dniAlto = 0;

		/*
		// Lee Fichero
		numEmpleados = leerFichero(nombreEmp, dniEmp);
		
		// Escribe Fichero
		for (int i = 0; i < numEmpleados; i++)
			System.out.println(nombreEmp[i] + " DNI: " + dniEmp[i]);
		
		// Saca DNI mas Alto 
		for (int i = 0; i < numEmpleados; i++) {
			if (dniEmp[i] > dniEmp[dniAlto])
				dniAlto = i;
			if (i == numEmpleados - 1)
				System.out.println(nombreEmp[dniAlto] + " tiene el DNI m�s alto");
		}
		
		// Saca DNI mas Bajo
		for (int i = 0; i < numEmpleados; i++) {
			if (dniEmp[i] < dniEmp[dniBajo])
				dniBajo = i;
			if (i == numEmpleados - 1)
				System.out.println(nombreEmp[dniBajo] + " tiene el DNI m�s bajo");
		}
		*/
		
		// Lee Fichero
		numEmpleados = leerFichero (empleados);
		
		// Escribe Fichero
		for (int i = 0; i < numEmpleados; i++)
			empleados[i].escribir();

		// Saca DNI mas Alto 
		for (int i = 0; i < numEmpleados; i++) {
			if (empleados[i].getDNI() > empleados[dniAlto].getDNI())
				dniAlto = i;
			if (i == numEmpleados - 1)
				System.out.println(empleados[dniAlto].getNombre() + " tiene el DNI m�s alto");
		}
		
		// Saca DNI mas Bajo
		for (int i = 0; i < numEmpleados; i++) {
			if (empleados[i].getDNI() < empleados[dniBajo].getDNI())
				dniBajo = i;
			if (i == numEmpleados - 1)
				System.out.println(empleados[dniBajo].getNombre() + " tiene el DNI m�s alto");
		}
	}

	//public static int leerFichero(String[] nomEmp, int[] dniEmp) {
	public static int leerFichero(Empleado[] empleados) {
		int c;
		int numEmp = 0;
		int pos = 0, emp = 0;
		String cadena = "";

		try {
			File fichero = new File(".//src//Examen//examen.txt");
			FileReader fr = new FileReader(fichero);

			c = fr.read();

			while (c != -1) {
				if (c != '-') {
					cadena += (char) c;
				} else {
					if (pos == 0) {
						numEmp = Integer.parseInt(cadena);
						// No se puede hacer aqui un new
						// MAL --> nomEmp = new String[numEmp];
						// MAL --> dniEmp = new int[numEmp];
						// MAL --> empleados = new Empleado [numEmp];
					} else if (pos % 2 == 1) {
						//nomEmp[emp] = cadena;
						empleados[emp].addNombre(cadena);
					} else if (pos % 2 == 0) {
						//dniEmp[emp] = Integer.parseInt(cadena);
						empleados[emp].addDNI( Integer.parseInt(cadena) );						
						emp++;
					}
					cadena = "";
					pos++;
				}
				c = fr.read();
			}
			fr.close();
		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		return numEmp;
	}
}
