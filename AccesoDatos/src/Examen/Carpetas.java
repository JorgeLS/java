package Examen;

import java.io.*;
import java.util.*;

public class Carpetas {

	// Lee dos numeros por pantalla, crea una carpeta llamada igual que
	// el segundo n�mero y crea despues dentro de esta carpeta un fichero
	// .num con cada numero existente entre cada uno de los n�meros.
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		int num1 = 0;
		int num2 = 0;

		// Lee los numeros
		System.out.println("Dime el primer numero: ");
		num1 = sc.nextInt();
		System.out.println("Dime el segundo numero: ");
		num2 = sc.nextInt();
		
		// Crea la carpeta
		// File carpeta = new File(num2 + "");
		File carpeta = new File(Integer.toString(num2));

		if (carpeta.mkdir() == true) {
			try {
				// Crea todos los ficheros
				for (int i = num1; i <= num2; i++) {
					File fichero = new File(carpeta, i + ".num");
					fichero.createNewFile();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		sc.close();
	}
}