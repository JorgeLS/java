package PrimerasClases;

public class Metodos {

	public static void main(String[] args)
	{
		Numero num = new Numero(3);
		
		System.out.println("La suma de 1 + 2 = " + suma(1, 2));
		System.out.println("La suma de 2 + 3 = " + suma(2, num));
		System.out.println("Sumamos al objeto ("+ num.getValor() +") a 5 = " + num.Sumar(5));
		System.out.println("Sumamos dos numeros = " + Numero.Sumar(num.getValor(), 3));
		
		System.out.print("Sumamos 4 al numero del objeto, que ahora es = ");
		num.sumValor(4);
		System.out.println(num.getValor());
	}
	
	public static int suma (int op1, int op2)
	{
		return op1 + op2;
	}
	
	public static int suma (int op1, Numero num) 
	{
		return op1 + num.getValor();
	}
}
