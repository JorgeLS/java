package PrimerasClases;
public class Numero
{
	private int valor;
	
	// Metodos
	public Numero(int valor)
	{
		this.valor = valor;
	}
	
	// Suma el valor a un nuevo numero (parametro)
	public int Sumar (int op1)
	{
		return op1 + this.valor;
	}
	
	// Retorna el valor
	public int getValor ()
	{
		return this.valor;
	}
	
	public void sumValor (int num)
	{
		this.valor += num;
	}
	
	public static int Sumar (int op1, int op2)
	{
		// NO this.valor
		// No se puede acceder al valor ya que todav�a no se a creado el objeto

		return op1 + op2;
	}	
}