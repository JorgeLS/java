package Ficheros;

import java.io.*;

public class Ejercicio1CFichero {
	private String ruta;
	private File fichero;
	
	public Ejercicio1CFichero (String ruta) {
		this.ruta = ruta;
		fichero = new File(ruta);		
	} 

	public void escribirFichero (int[] lista)
	{
		try
		{
			FileWriter fw = new FileWriter(fichero);
			String s;
			
			for (int i = 0; i < lista.length; i++)
			{
				s = Integer.toString(lista[i]) + "\n";
				fw.write(s);
			}
			
			fw.close();
		}
		catch (IOException e)
		{
			System.out.println("FileWrite: " + e.getMessage());
		}	
	}
	
	public int[] leerFicheroString ()
	{
		String txt;
		int[] lista = new int[10];
		int pos = 0;
		
		try
		{
			FileReader fw = new FileReader(fichero);
			BufferedReader br = new BufferedReader(fw);
			
			while ( (txt = br.readLine()) != null && pos != 10)
			{
				lista[pos++] = Integer.parseInt(txt);
			}
			
			br.close();
		}
		catch (IOException e)
		{
			System.out.println("FileWrite: " + e.getMessage());
		}
		
		return lista;
	}
	
	public int[] leerFicheroCaracteres ()
	{
		int[] lista = new int[10];
		int num = 0;
		int pos = 0;
		int resultado = 0;
		
		try
		{
			FileReader fw = new FileReader(fichero);
			
			resultado = fw.read();
			
			while ( resultado != -1 && pos != 10)
			{
				if (resultado == '\n') {
					lista[pos++] = num;
					num = 0;
				}
				else {
					if (num == 0)
						num = Character.getNumericValue(resultado);
					else
						num = num * 10 + Character.getNumericValue(resultado);
				}
				resultado = fw.read();
			}
			
			fw.close();
		}
		catch (IOException e)
		{
			System.out.println("FileWrite: " + e.getMessage());
		}
		
		return lista;
	}
	
	public void actualizarFichero (int[] lista)
	{
		try
		{
			FileWriter fw = new FileWriter(fichero);
			String s;
			
			for (int i = 0; i < lista.length; i++)
			{
				s = Integer.toString(lista[i]) + "\n";
				fw.write(s);
			}
			
			fw.close();
		}
		catch (IOException e)
		{
			System.out.println("FileWrite: " + e.getMessage());
		}	
	}
}
