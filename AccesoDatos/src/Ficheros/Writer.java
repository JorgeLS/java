package Ficheros;

import java.io.*;

public class Writer {

	public static void main(String[] args) {
		File fichero = new File(".//src//txtFicheros//ficheroTexto1.txt");
		
		escribeFileW(fichero);
		
	}
	
	public static void escribeFileW (File fichero)
	{		
		try
		{			
			FileWriter fw = new FileWriter(fichero);
			
			fw.write("Pepe\n");
			fw.write("Ana\n");
			fw.write("Juan\n");
			
			fw.close();
		} catch (IOException e)
		{
			System.out.println("No se ha podido cargar.");
		}		
	}
}
