package Ficheros;

import java.io.*;

public class CreadorProgramas {
	
	protected static String[] rutas =
	{
			"Proyecto",
			"Proyecto\\bin",
			"Proyecto\\bin\\bytecode",
			"Proyecto\\bin\\objetos",
			"Proyecto\\src",
			"Proyecto\\src\\clases",
			"Proyecto\\doc",
			"Proyecto\\doc\\html",
			"Proyecto\\doc\\pdf"
	};
	
	public static void main (String[] args) {
		
		boolean seguir = true;
		for (int i = 0; i < rutas.length && seguir; i++) {
			File carpeta = new File(rutas[i]);
			seguir = carpeta.mkdir();
			if (!seguir)
				System.out.println("Ya esta creado la carpeta: " + rutas[i]);
		}		
	}
}
