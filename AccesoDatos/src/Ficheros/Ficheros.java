package Ficheros;

import java.io.*;

public class Ficheros {
	
	public static void main (String[] args) {
		
		File carpeta = new File("Ficheros");
				
		if (carpeta.mkdir() == true) {
			carpeta.mkdir();
			System.out.println("Carpeta creada.");
		}
		else
			System.out.println("La carpeta ya est� creada.");
		
		File fich1 = new File(carpeta, "primero.txt");
		File fich2 = new File(carpeta, "segundo.txt");
			
		try 
		{
			fich1.createNewFile();
			fich2.createNewFile();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}
}
