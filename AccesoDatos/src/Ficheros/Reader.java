package Ficheros;

import java.io.*;

public class Reader
{

	public static void main(String[] args)
	{
		File fichero = new File(".//src//txtFicheros//ficheroTexto1.txt");
		String[] listaNom = new String [10];
		
		for (int i = 0; i < listaNom.length; i++)
			listaNom[i] = "";
		
		//System.out.println(leerFicheroCC (fichero));
		leerFicheroCS (fichero, listaNom);
		//System.out.println(leerFicheroCS (fichero));
		
		for (int i = 0; i < listaNom.length; i++) {
			if (listaNom[i] == "")
				continue;
			else
				System.out.println(i + ".- " + listaNom[i]);
		}
		
	}
	
	public static String leerFicheroCC (File fichero)
	{
		
		String cadena = "";
		
		try
		{
			FileReader fr = new FileReader(fichero);
			
			int letra;
			
			while ( (letra = fr.read()) != -1 ) 
			{
				cadena += (char) letra;
			}
			
			fr.close();
			
		} catch (IOException e)
		{
			System.out.println("Ho se ha encontrado el fichero: " + e.getMessage());
		}
		
		return cadena;
	}
	
	public static void leerFicheroCS (File fichero, String[] lista)
	{
		int pos = 0;
		int corte = 4;
		char[] caracter = new char[4];
		
		try
		{
			FileReader fr = new FileReader(fichero);
			
			while ( (fr.read(caracter, 0, 4)) != -1)
			{				
				for (int i = 0; i < caracter.length; i++)
				{
					if (caracter[i] != '\n' && pos < corte)
						lista[pos] += caracter[i];
					else
						pos++;
				}
			}
			
			fr.close();
			
		} catch (IOException e)
		{
			System.out.println("Ho se ha encontrado el fichero: " + e.getMessage());
		}
	}
}
