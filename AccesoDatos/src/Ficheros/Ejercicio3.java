package Ficheros;

import java.io.*;
import java.util.*;

public class Ejercicio3 {

	public static void main(String[] args) {

		Persona[] persona = {
				new Persona("Guille", "Albatros", 19),
				new Persona("BoJack", "Horseman", 22),
				new Persona("Ricardo", "Sanchez", 34),
				new Persona("Jorge", "Loparte", 54),
				new Persona("David", "TmbLoparte", 10)
				};

		int op = 0;
		Scanner sc = new Scanner(System.in);
		File fichero = new File(".//src//txtFicheros//ejercicio3.txt");

		do {

			System.out.print("Dime una opcion: ");
			op = sc.nextInt();

			switch (op) {
			case 0:
				escribirFichero(fichero, persona);
				break;
			case 1:
				leerFichero(fichero, persona);
				break;
			case 2:
				if (persona[0] != null)
					for (int i = 0; i < persona.length; i++)
						System.out.println(persona[i].toString(i));
				else
					System.out.println("Persona esta vacia.");
				break;
			case 3:
				persona = new Persona[5];
				break;
			case 4:
				System.out.println("Adeu.");
				break;
			default:
				System.out.println("Opcion no valida.");
				break;
			}
		} while (op != 4);
		
		sc.close();
	}

	private static void escribirFichero(File fichero, Persona[] persona) {

		try {
			FileWriter fw = new FileWriter(fichero);
			BufferedWriter escritor = new BufferedWriter(fw);

			for (int i = 0; i < persona.length; i++) {
				escritor.write(persona[i].toString(i) + "\n");
			}
			escritor.close();
			
		} catch (IOException e) {
			System.out.println("Error:\n" + e.getMessage());
		}
	}
	

	private static void leerFichero(File fichero, Persona[] persona) {

		String texto = "";
		int pos = 0;
		
		try {
		FileReader fr = new FileReader(fichero);
		BufferedReader lector = new BufferedReader(fr);
		
		while ((texto = lector.readLine()) != null) {
			String nombre = texto.substring(texto.indexOf(pos + ":") + 2, texto.indexOf(" -"));
			String apellido = texto.substring(texto.indexOf(" - ") + 3, texto.indexOf(" de edad"));
			int edad = Integer.parseInt(texto.substring(texto.indexOf(" de edad ") + 9, texto.indexOf('.')));

			persona[pos] = new Persona(nombre, apellido, edad);
			pos++;
		}

		lector.close();

		} catch (IOException e) {
			System.out.println("Error:\n" + e.getMessage());
		}
	}
}
