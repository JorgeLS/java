package Ficheros;

public class Ejercicio1CLista {
	
	private int[] lista;
	private int pos;
	
	public Ejercicio1CLista (int numElementos) {
		this.lista = new int [numElementos];
		this.pos = 0;
	}
	
	public Ejercicio1CLista (int[] lista) {
		this.lista = lista;
		this.pos = lista.length;
	}
	
	public int getLength () {
		return pos;
	}
	
	public void insetaNum (int valor) {
		if (pos == 10)
			lista[pos++] = valor;							
	}
	

	public void setElemento (int posicion, int valor) {
		this.lista[posicion] = valor;
	}
	public int getElemento (int posicion) {
		return lista[posicion];
	}
	
	public void setLista (int[] lista) {
		this.lista = lista;
		pos = lista.length;
	}
	public int[] getLista () {
		return lista;
	}
}
