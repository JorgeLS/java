package Ficheros;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Ejercicio2
{
	
	public static void main(String[] args)
	{
		String[] listaNom = {"Pedro", "Pepe", "Ana", "Koke", "Terco", "Jorge"};
		File fichero = new File(".//src//txtFicheros//ejercicio2.txt");

		escribirFichero (listaNom, fichero);
		
		inicializacionTabla (listaNom);
		leerFichero(listaNom, fichero);

		escribirTabla (listaNom);
	}

	public static void inicializacionTabla (String[] lista)
	{
		for (int i = 0; i < lista.length; i++)
			lista[i] = "";
	}
	public static void escribirTabla (String[] lista)
	{
		System.out.println("Tabla:");
		for (int i = 0; i < lista.length; i++)
			System.out.println(i + ".- " + lista[i]);
	}
	
	public static void escribirFichero (String[] lista, File fichero)
	{
		try
		{
			FileWriter fw = new FileWriter(fichero);
			
			for (int i = 0; i < lista.length; i++)
			{
				fw.write(lista[i] + "\n");
			}
			
			fw.close();
		}
		catch (IOException e)
		{
			System.out.println("FileWrite: " + e.getMessage());
		}	
	}
	
	public static void leerFichero (String[] lista, File fichero)
	{
		int pos = 0;
		int caracter;
		
		try
		{
			FileReader fw = new FileReader(fichero);
			
			while ( (caracter = fw.read()) != -1 )
			{
				if (caracter != '\n')
					lista[pos] += (char) caracter;
				else
					pos++;
			}
						
			fw.close();
		}
		catch (IOException e)
		{
			System.out.println("FileWrite: " + e.getMessage());
		}
	}		
}