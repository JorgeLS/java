package Ficheros;

import java.io.*;
import java.util.*;

public class Ejercicio1
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		int op = 0;
		
		int[] lista = {10, 21, 62, 3, 34, 7, 9, 22, 5, 1};

		Ejercicio1CFichero fichero = new Ejercicio1CFichero(".//src//txtFicheros//ejercicio1.txt");
		Ejercicio1CLista listaNum = new Ejercicio1CLista(10);
		//Ejercicio1CLista listaNum = new Ejercicio1CLista(lista);
		do {
			
			System.out.print("Dime una opcion: ");
			op = sc.nextInt();
			
			switch(op)
			{
				case 0:
					if (listaNum.getLength() == 10)
						fichero.escribirFichero ( listaNum.getLista() );
					else
						System.out.println("No esta rellena la tabla.");
					break;
				case 1:
					listaNum.setLista( fichero.leerFicheroCaracteres() );
					break;
				case 2:
					for (int i = 0; i < listaNum.getLength(); i++)
					{
						System.out.println(i + ".- " + listaNum.getElemento(i));
					}						
					break;
				case 3:
					System.out.println("Dime posicion tabla: ");
					int pos = sc.nextInt();
					System.out.println("Dime dato a introducir: ");					
					int dato = sc.nextInt();
					
					listaNum.setElemento(pos, dato);
					break;
				case 4:
					fichero.actualizarFichero ( listaNum.getLista() );
					break;
				case 5:
					System.out.println("Adeu.");
					break;
			}
		} while (op != 5);
	}
}